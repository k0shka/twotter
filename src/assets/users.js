export const users = [
        {
            "id": 1,
            "username": "_jops",
            "firstName": "John",
            "lastName": "Doe",
            "email": "johndoe@yahoo.com",
            "isAdmin": true,
            "twoots": [
                { "id": 1, "content": "Twotter is Amazing!"},
                { "id": 2, "content": "Don't forget to subscribe to my channel"},
            ]
        },
        {
            "id": 2,
            "username": "_supreme1",
            "firstName": "Adam",
            "lastName": "Max",
            "email": "supreme1@yahoo.com",
            "isAdmin": false,
            "twoots": []
        },
        {
            "id": 3,
            "username": "_ph",
            "firstName": "Philip",
            "lastName": "Pines",
            "email": "ph@yahoo.com",
            "isAdmin": false,
            "twoots": []
        },
    ]
